variable "ecr_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

variable "ecr_name" {
  description = "Name of the repository."
  type        = string
}
variable "ecr_tags" {
  description = "A mapping of tags to assign to the resources."
  type        = map(string)
  default     = {}
}
variable "ecr_public" {
  description = "Will the repository be publicly accessible."
  type        = bool
  default     = true
}


variable "ecr_admins" {
  description = "ARNs of IAM entities full access to the ECR will be provided to."
  type        = list(string)
  default     = []
}

variable "ecr_operators" {
  description = "ARNs of IAM entities read-only access to the ECR will be provided to."
  type        = list(string)
  default     = []
}

variable "ecr_untagged_image_count" {
  description = "How many untagged images will be stored before AWS ECR starts to remove oldest ones."
  default     = 5
}

variable "ecr_max_image_count" {
  description = "How many Docker Image versions AWS ECR will store."
  default     = 15
}
