locals {
  enabled = var.ecr_module_enabled ? true : false
  tags = merge(
    var.ecr_tags,
    {
      Name      = format("%s", var.ecr_name)
      terraform = "true"
    },
  )
}

resource "aws_ecr_repository" "repository" {
  # Provides an Elastic Container Registry Repository.
  count = local.enabled ? 1 : 0

  # (Required) Name of the repository.
  name = var.ecr_name
  # (Optional) A mapping of tags to assign to the resource.
  tags = local.tags
}

resource "aws_ecr_lifecycle_policy" "lifecycle_policy" {
  # Manages an ECR repository lifecycle policy.
  # NOTE(!): Only one `aws_ecr_lifecycle_policy` resource can be used with the same ECR repository. To apply
  #          multiple rules, they must be combined in the policy JSON.
  # NOTE(!!): The AWS ECR API seems to reorder rules based on `rulePriority`. If you define multiple rules that are not
  #           sorted in ascending `rulePriority` order in the Terraform code, the resource will be flagged for
  #           recreation every terraform plan.
  count = local.enabled ? 1 : 0

  #  (Required) Name of the repository to apply the policy.
  repository = join("", aws_ecr_repository.repository.*.name)
  # (Required) The policy document. This is a JSON formatted string. See more details about Policy Parameters in the
  # official AWS docs:
  #   * http://docs.aws.amazon.com/AmazonECR/latest/userguide/LifecyclePolicies.html#lifecycle_policy_parameters
  policy = <<EOF
{
  "rules": [
    {
      "rulePriority": 10,
      "description": "remove-untagged-images-upper-specified-limit",
      "selection": {
        "tagStatus": "untagged",
        "countType": "imageCountMoreThan",
        "countNumber": ${var.ecr_untagged_image_count}
      },
      "action": {
        "type": "expire"
      }
    },
    {
      "rulePriority": 20,
      "description": "rotate-images-when-limit-of-total-images-${var.ecr_max_image_count}-reached",
      "selection": {
        "tagStatus": "any",
        "countType": "imageCountMoreThan",
        "countNumber": ${var.ecr_max_image_count}
      },
      "action": {
        "type": "expire"
      }
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "operators_access" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use
  # with resources which expect policy documents, such as the `aws_iam_policy` resource.
  count = local.enabled ? 1 : 0

  statement {
    sid    = "ReadOnlyRepositoryAccess"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = var.ecr_operators
    }
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:DescribeImages",
      "ecr:DescribeRepositories",
      "ecr:GetAuthorizationToken",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:ListImages",
    ]
  }
}

data "aws_iam_policy_document" "admin_access" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use
  # with resources which expect policy documents, such as the `aws_iam_policy` resource.
  count = local.enabled ? 1 : 0

  statement {
    sid    = "FullRepositoryAccess"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = var.ecr_admins
    }
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:CompleteLayerUpload",
      "ecr:DescribeImages",
      "ecr:DescribeRepositories",
      "ecr:GetAuthorizationToken",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:InitiateLayerUpload",
      "ecr:ListImages",
      "ecr:PutImage",
      "ecr:UploadLayerPart",
    ]
  }
}

data "aws_iam_policy_document" "public_access" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use
  # with resources which expect policy documents, such as the `aws_iam_policy` resource.
  count = local.enabled ? 1 : 0

  statement {
    sid    = "PublicAccess"
    effect = "Allow"
    # See Anonymous Users (Public) section at:
    #   * https://docs.aws.amazon.com/en_us/IAM/latest/UserGuide/reference_policies_elements_principal.html
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:DescribeImages",
      "ecr:DescribeRepositories",
      "ecr:GetAuthorizationToken",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:ListImages",
    ]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "empty" {
  count = local.enabled ? 1 : 0
}

data "aws_iam_policy_document" "resulting_access" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use
  # with resources which expect policy documents, such as the `aws_iam_policy` resource.
  count = local.enabled ? 1 : 0

  # (Optional) - An IAM policy document to import as a base for the current policy document.
  # Statements with non-blank SIDs in the `current` policy document will overwrite statements with the same SID in
  # the `source` JSON.
  # Statements without an SID cannot be overwritten.
  source_json = length(var.ecr_operators) > 0 ? join("", data.aws_iam_policy_document.operators_access.*.json) : (var.ecr_public ? join("", data.aws_iam_policy_document.public_access.*.json) : join("", data.aws_iam_policy_document.empty.*.json))

  # (Optional) - An IAM policy document to import and override the current policy document.
  # Statements with non-blank SIDs in the `override` document will overwrite statements with the same SID in
  # the `current` document.
  # Statements without an SID cannot be overwritten.
  override_json = length(var.ecr_admins) > 0 ? join("", data.aws_iam_policy_document.admin_access.*.json) : (var.ecr_public ? join("", data.aws_iam_policy_document.public_access.*.json) : join("", data.aws_iam_policy_document.empty.*.json))
}

resource "aws_ecr_repository_policy" "repository_policy" {
  # Provides an Elastic Container Registry Repository Policy.
  # NOTE(!): that currently only one policy may be applied to a repository.
  count = (local.enabled && (length(var.ecr_admins) + length(var.ecr_operators) > 0)) ? 1 : 0

  # (Required) Name of the repository to apply the policy.
  repository = join("", aws_ecr_repository.repository.*.name)
  # (Required) The policy document. This is a JSON formatted string.
  policy = join("", data.aws_iam_policy_document.resulting_access.*.json)
}
