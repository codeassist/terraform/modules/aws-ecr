output "repository_arn" {
  description = "Full ARN of the repository."
  value       = local.enabled ? join("", aws_ecr_repository.repository.*.arn) : ""
}

output "repository_name" {
  description = "The name of the repository."
  value       = local.enabled ? join("", aws_ecr_repository.repository.*.name) : ""
}

output "registry_id" {
  description = "The registry ID where the repository was created."
  value       = local.enabled ? join("", aws_ecr_repository.repository.*.registry_id) : ""
}

output "repository_url" {
  description = "The URL of the repository (in the form <aws_account_id>.dkr.ecr.<aws_region>.amazonaws.com/<repository_name>)"
  value       = local.enabled ? join("", aws_ecr_repository.repository.*.repository_url) : ""
}
